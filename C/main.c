/*
 * main.c
 */
#include <stdio.h>

int main(int argc, char * argv[])
{
    int i = 0, sum = 0;

    for (; i <= 10; i++) {
        sum += i;
        printf("i = %d, sum = %d\n", i, sum);
    }

    return 0;
}
